package syncmaplocker

import (
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
)

func TestSyncMapLocker(t *testing.T) {
	concurrency := 3
	tries := 10

	locker := NewSyncMapLocker(tries)

	wg := new(sync.WaitGroup)
	wg.Add(concurrency * tries)

	for c := 0; c < concurrency; c++ {
		for i := 0; i < tries; i++ {
			id := i

			go func() {
				defer wg.Done()

				locker.Lock(id)
				defer locker.Unlock(id)
			}()
		}
	}

	wg.Wait()

	assert.Equal(t, 0, len(locker.useMap))
	assert.Equal(t, 0, len(locker.referenceCountMap))
}

func TestSyncMapLockerMulti(t *testing.T) {
	concurrency := 5

	id := 1

	locker := NewSyncMapLocker(1)

	wg := new(sync.WaitGroup)
	wg.Add(concurrency)

	for i := 0; i < concurrency; i++ {
		go func() {
			wg.Done()

			locker.Lock(id)
		}()
	}

	wg.Wait()

	wg = new(sync.WaitGroup)
	wg.Add(concurrency)

	for i := 0; i < concurrency; i++ {
		go func() {
			defer wg.Done()

			locker.Unlock(id)
		}()
	}

	wg.Wait()

	assert.Equal(t, 0, len(locker.useMap))
	assert.Equal(t, 0, len(locker.referenceCountMap))
}

func BenchmarkSyncMapLocker(b *testing.B) {
	concurrency := 10

	locker := NewSyncMapLocker(concurrency)

	wg := new(sync.WaitGroup)
	wg.Add(b.N)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		id := i % concurrency

		go func() {
			defer wg.Done()

			locker.Lock(id)
			defer locker.Unlock(id)
		}()
	}

	wg.Wait()
	b.StopTimer()

	assert.Equal(b, 0, len(locker.useMap))
	assert.Equal(b, 0, len(locker.referenceCountMap))
}
