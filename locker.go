package syncmaplocker

import (
	"fmt"
	"sync"
)

type SyncMapLocker struct {
	mu                *sync.Mutex
	useMap            map[int]*sync.Mutex
	referenceCountMap map[int]int
}

func (l *SyncMapLocker) Lock(id int) {
	l.up(id).Lock()
}

func (l *SyncMapLocker) Unlock(id int) {
	l.down(id).Unlock()
}

func (l *SyncMapLocker) up(id int) *sync.Mutex {
	l.mu.Lock()
	defer l.mu.Unlock()

	mu, ok := l.useMap[id]
	if ok {
		// read and set
		l.referenceCountMap[id]++
	} else {
		mu = new(sync.Mutex)

		l.useMap[id] = mu

		// only set
		l.referenceCountMap[id] = 1
	}

	return mu
}

func (l *SyncMapLocker) down(id int) *sync.Mutex {
	l.mu.Lock()
	defer l.mu.Unlock()

	mu, ok := l.useMap[id]
	if !ok {
		panic(fmt.Sprintf("undefined id %d", id))
	}

	if l.referenceCountMap[id] > 1 {
		l.referenceCountMap[id]--
	} else {
		delete(l.useMap, id)
		delete(l.referenceCountMap, id)
	}

	return mu
}

func NewSyncMapLocker(size int) *SyncMapLocker {
	return &SyncMapLocker{
		mu:                new(sync.Mutex),
		useMap:            make(map[int]*sync.Mutex, size),
		referenceCountMap: make(map[int]int, size),
	}
}
