# SyncMapLocker

Lock some operation by concurrent changes

##### Example
```golang
    type OrderService struct {
        locker *SyncMapLocker
    }
    
    func (s *OrderService) AddBasket(orderId int, ...) {
        s.locker.Lock(orderId)
        defer s.locker.Unlock(orderId)
        
        ...
    }
    
    func (s *OrderService) DeleteBasket(orderId int, ...) {
        s.locker.Lock(orderId)
        defer s.locker.Unlock(orderId)
        
        ...
    }
```

##### Benchmark
```bash
BenchmarkSyncMapLocker-4         1000000              1429 ns/op             226 B/op          0 allocs/op
```

